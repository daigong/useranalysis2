#encoding=utf-8
class DataUtils

  def self.concat_string string1, string2
    string1.concat string2
    string1 = "Unknown" if string1.blank?
    string1
  end

end