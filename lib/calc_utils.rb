#encoding=utf-8
class CalcUtils

  def self.calc_percentage_to_s num1, num2, smth=2
    if num2==0
      return "NaN"
    end
    percent = (num1.to_f/num2.to_f)*100
    "#{percent.round(smth)}%"
  end

  def self.calc_percentage_to_i num1, num2, smth=2
     percent = (num1.to_f/num2.to_f)*100
     percent.round(smth)
  end

  def self.calc_percentage_revert_to_s num1, num2, smth=2
    percent = (num1.to_f/num2.to_f)*100
    "#{100 - percent.round(smth)}%"
  end

end