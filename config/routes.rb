UserAnalysis::Application.routes.draw do

  root :to => 'statistics/total#index'
  ####基本统计##################################################

  namespace :statistics do

    #
    get 'total' => 'total#index'


    scope :path=>':app_id' do

      root :to => 'main#index'
      #每日明细_分页信息
      get 'daily_stats_details_page'=>'main#daily_stats_details_page'

      get 'total_active_info_page' =>'main#total_active_info_page'
      ####版本分布##################################################

      get 'app_version' => 'appVersion#index'

      ####回访用户##################################################

      get 'revisit' => 'revisit#index'
      #回访用户详情分页
      get 'daily_revisit_details_page' => 'revisit#daily_revisit_details_page'

      get 'revisit' => 'revisit#index'

      ####地域#####################################################

      get 'location' => 'location#index'
      #省分布分页
      get 'location_province_details_page' => 'location#location_province_details_page'

      ####启动次数##################################################
      get 'launch' => 'launch#index'
      ####使用时长##################################################
      get 'usetime' => 'usetime#index'
      ####设备######################################################
      get 'device' => 'device#index'
      #设备信息详情分页
      get 'device_details_page' => 'device#device_details_page'
      ####操作系统##################################################
      get 'os' => 'os#index'
      #操作系统信息详情分页
      get 'os_details_page' => 'os#os_details_page'
      ####分辨率####################################################
      get 'resolution' => 'resolution#index'
      #操作系统信息详情分页
      get 'resolution_details_page' => 'resolution#resolution_details_page'
      ####运营商####################################################
      get 'carrier' => 'carrier#index'
      #运营商信息详情分页
      get 'carrier_details_page' => 'carrier#carrier_details_page'
      ####联网方式##################################################
      get 'access' => 'access#index'

      get 'channels' => 'channels#index'

      scope :path=>':channel_id' do
        get 'channel_detail' => 'channel_detail#index'
        get 'more_details_page' => 'channel_detail#more_details_page'
      end

      resources :except_info

      resources :users

      resources :except_version

      resources :except_day

      resources :debug_info

    end
  end

  ####log分析##################################################
  namespace :loganalysis do


    get 'analysis' => 'analysis#index'

    get 'response' => 'response#index'

    get 'busline' => 'busline#index'

    get 'busline_details_page' => 'busline#busline_details_page'

    get 'busstop' => 'busstop#index'

    get 'busstop_details_page' => 'busstop#busstop_details_page'

    get 'rg' => 'rg#index'

    get 'rg_details_page' => 'rg#rg_details_page'

    get 'travel_bus' => 'travel_bus#index'

    get 'travel_bus_details_page' => 'travel_bus#travel_bus_details_page'

    get 'travel_walk' => 'travel_walk#index'

    get 'travel_walk_details_page' => 'travel_walk#travel_walk_details_page'

    get 'poi_name' => 'poi_name#index'

    get 'poi_name_details_page' => 'poi_name#poi_name_details_page'

    get 'poi_around' => 'poi_around#index'

    get 'poi_around_details_page' => 'poi_around#poi_around_details_page'

    get 'poi_area' => 'poi_area#index'

    get 'poi_area_details_page' => 'poi_area#poi_area_details_page'

    get 'poi_city' => 'poi_city#index'

    get 'poi_city_details_page' => 'poi_city#poi_city_details_page'

    get 'eeye' => 'eeye#index'

    get 'eeye_details_page' => 'eeye#eeye_details_page'

    get 'mm' => 'mm#index'

    get 'mm_details_page' => 'mm#mm_details_page'

    get 'view' => 'view#index'

    get 'view_details_page' => 'view#view_details_page'

    get 'losepoi' => 'losepoi#index'


  end

  #########信息服务###################################################
  namespace :service do
    get 'get_user_info'=>'UserInfoService#get_user_info'
  end


  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id(.:format)))'
end
