(function() {
  var CoffeeAdapterConfig, PageBase;

  CoffeeAdapterConfig = (function() {

    function CoffeeAdapterConfig() {
      this.classes = {};
      return;
    }

    CoffeeAdapterConfig.prototype.addPageClass = function(classNameWithNameSpace, classFunction) {
      this.classes[classNameWithNameSpace] = classFunction;
    };

    CoffeeAdapterConfig.prototype.getPageClass = function(classNameWithNameSpace) {
      return this.classes[classNameWithNameSpace];
    };

    CoffeeAdapterConfig.prototype.getClassName = function(fn) {
      var name;
      if (fn.name != null) return fn.name;
      name = /\W*function\s+([\w\$]+)\(/.exec(fn);
      if (!name) {
        log.error("没有找到函数名字:" + fn.toString());
        return 'no name';
      }
      return name[1];
    };

    CoffeeAdapterConfig.prototype.clear = function() {
      return delete this.classes;
    };

    CoffeeAdapterConfig.prototype.version = '0.1.0';

    return CoffeeAdapterConfig;

  })();

  window._coffeeAdapterConfig = new CoffeeAdapterConfig();

  window.registerClass = function(pageClass, namespace) {
    var class_name, name_with_namespace;
    class_name = window._coffeeAdapterConfig.getClassName(pageClass);
    if (!namespace) {
      log.warn("注册 " + class_name + " 时没有指定namespace！");
      name_with_namespace = class_name;
    } else {
      name_with_namespace = namespace + "." + class_name;
    }
    if (window._coffeeAdapterConfig.getPageClass(name_with_namespace)) {
      return log.warn("类: " + name_with_namespace + " 已经在其他地方定义，请确认！");
    } else {
      window._coffeeAdapterConfig.addPageClass(name_with_namespace, pageClass);
      return log.info("类： " + name_with_namespace + " 注册成功 ！");
    }
  };

  window.$Class = function() {
    var clazz, message;
    log.debug("查找class：" + arguments[0]);
    clazz = window._coffeeAdapterConfig.getPageClass.apply(window._coffeeAdapterConfig, arguments);
    if (!clazz) {
      message = "没有找到：" + arguments[0] + "类，请确认是否在使用前注册了该类？";
      log.error(message + " 请查看异常。");
      throw TypeError(message);
    }
    return clazz;
  };

  PageBase = (function() {

    function PageBase() {
      return;
    }

    PageBase.prototype.init = function() {
      var message;
      message = "请覆盖PageBase类的初始化方法为页面进行初始化！";
      log.error(message + "，请查看异常信息");
      throw TypeError(message);
    };

    return PageBase;

  })();

  window.registerClass(PageBase);

  $(function() {
    var $body, pageClass, pageClassFunction, pageParams, params;
    $body = $('body');
    pageClass = $body.attr('data-page-class');
    if (!pageClass) {
      log.error("该页面body中没有找到data-page-class属性。");
      return;
    }
    pageClassFunction = window._coffeeAdapterConfig.getPageClass(pageClass);
    if (!pageClassFunction) {
      log.error("未找到页面body[data-page-class]配置的类: " + pageClass);
      return;
    }
    pageParams = $body.attr('data-page-params');
    params = [];
    if (pageParams && pageParams !== '') eval("params = " + pageParams);
    log.debug("页面配置的body[data-page-class]为 " + pageClass + "，页面初始化参数为 " + params + "，开始初始化...");
    window._pageObject = new pageClassFunction();
    if (window._pageObject.init) {
      window._pageObject.init.apply(window._pageObject, params);
      log.info("页面body[data-page-class]配置的类:" + pageClass + ",页面初始化参数为 " + params + " 成功初始化。");
    } else {
      log.error("页面中配置的body[data-page-class]为： " + pageClassFunction + " ，但是没有提供init方法。");
    }
    window._coffeeAdapterConfig.clear();
  });

}).call(this);
