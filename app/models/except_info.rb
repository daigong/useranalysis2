#encoding=utf-8

class ExceptInfo
  include Mongoid::Document
  set_database :log_info
  store_in :mobile_exceptinfo

  key :repair, :type=>Integer
  key :uuid, :type=>String
  key :exception_log, :type=>String
  key :soft_version_code, :type=>Integer
  key :soft_version, :type=>String
  key :key, :type=>String
  key :time, :type=>Time

end
