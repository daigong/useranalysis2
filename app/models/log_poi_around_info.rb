#encoding=utf-8
class LogPoiAroundInfo
  include Mongoid::Document
  set_database :log
  store_in :poi_around
  key :day_date, :type=>Time
  key :max_name, :type=>String
  key :mid_name, :type=>String
  key :count, :type=>Integer

end