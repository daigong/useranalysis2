#encoding=utf-8
class LoginCountInfo
  include Mongoid::Document
  store_in :mobile_logincountinfo

  key :key, :type=>String
  key :day_date, :type=>Time
  key :login_count_level0, :type=>Integer
  key :login_count_level1, :type=>Integer
  key :login_count_level2, :type=>Integer
  key :login_count_level3, :type=>Integer
  key :login_count_level4, :type=>Integer
  key :login_count_level5, :type=>Integer
  
end

