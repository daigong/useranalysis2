#encoding=utf-8
class LogPoiAreaInfo
  include Mongoid::Document
  set_database :log
  store_in :poi_area
  key :day_date, :type=>Time
  key :province, :type=>String
  key :city, :type=>String
  key :count, :type=>Integer


end