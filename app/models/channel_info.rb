#encoding=utf-8
class ChannelInfo
  include Mongoid::Document
  store_in :mobile_baseinfo_channel
  key :key, :type => String
  key :day_date, :type => Time
  key :channel, :type => String
  key :new_user_count, :type => Integer
  key :login_user_count, :type => Integer
  #key :yest_new_user_count, :type => Integer
  key :yest_login_user_count, :type => Integer
  key :login_count, :type => Integer
  key :yest_login_count, :type => Integer
  key :average_use_time, :type => Integer
  key :last_week_login_user_count, :type => Integer
  key :last_month_login_user_count, :type => Integer


  def self.get_total_by_day begin_day, end_day, chan, app_key
    return ChannelInfo.where(:day_date.gte => begin_day, :day_date.lte => end_day, :key => app_key, :channel.exists => true, :channel => chan)
  end

  def yest_new_user_count
    c = ChannelInfo.where(:day_date => (self.day_date-1.day), :key => self.key, :channel.exists => true, :channel => self.channel).first
    if c.nil?
      return 0
    else
      return c.new_user_count
    end
  end

  def yest_login_user_count
    c = ChannelInfo.where(:day_date => (self.day_date-1.day), :key => self.key, :channel.exists => true, :channel => self.channel).first
    if c.nil?
      return 0
    else
      return c.login_user_count
    end
  end

  def method_missing argc

    begin
      return_val = super argc
    rescue Exception => e
      return_val = 0
    end
    return return_val

  end
end