#encoding=utf-8
class LogLosePoiInfo
  include Mongoid::Document
  set_database :log
  store_in :server_losepois
  key :day_date, :type=>Time
  key :name, :type=>String
  key :city, :type=>String
  key :count, :type=>Integer

end