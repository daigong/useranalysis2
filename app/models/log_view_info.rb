#encoding=utf-8
class LogViewInfo
  include Mongoid::Document
  set_database :log
  store_in :view
  key :day_date, :type=>Time
  key :level, :type=>Integer
  key :province, :type=>String
  key :city, :type=>String
  key :count, :type=>Integer


end