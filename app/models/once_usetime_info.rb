#encoding=utf-8
class OnceUsetimeInfo
  include Mongoid::Document
  store_in :mobile_onceusetimeinfo
  key :key, :type=>String
  key :day_date, :type=>Time
  key :use_time_level0, :type=>Integer
  key :use_time_level1, :type=>Integer
  key :use_time_level2, :type=>Integer
  key :use_time_level3, :type=>Integer
  key :use_time_level4, :type=>Integer
  key :use_time_level5, :type=>Integer
  key :use_time_level6, :type=>Integer
  key :use_time_level7, :type=>Integer
end

