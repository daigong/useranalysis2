#encoding=utf-8
class DeviceInfo
  include Mongoid::Document
  store_in :mobile_deviceinfo

  key :key, :type=>String
  key :day_date, :type=>Time
  key :new_user_count, :type=>Integer
  key :login_user_count, :type=>Integer
  key :device_model, :type=>String
  
end