#encoding=utf-8
class ConnecType
  include Mongoid::Document
  store_in :mobile_connectype

  key :key, :type=>String
  key :day_date, :type=>Time
  key :new_user_count, :type=>Integer
  key :sum_user_count, :type=>Integer
  key :connect_type, :type=>String
end