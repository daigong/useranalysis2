#encoding=utf-8
class LoginInfo
  include Mongoid::Document
  set_database :log_info
  store_in :mobile_logininfo
  ##############################
  key :type, :type=>String
  key :key, :type=>String
  key :channel, :type=>String
  key :uuid, :type=>String
  key :time, :type=>Time
  field :os_version, :type => String
  field :soft_version, :type => String
  field :soft_version_code, :type => String
  field :device_model, :type => String
  field :resolution, :type => String
  field :sys_country, :type => String
  field :carrier, :type => String
  field :connect_type, :type => String
  field :connect_subtype, :type => String
  field :cellid, :type=>String
  field :bssid, :type=>String
  field :exception_log, :type=>String
end
