#encoding=utf-8
class LogRgInfo
  include Mongoid::Document
  set_database :log
  store_in :rg
  key :day_date, :type=>Time
  key :start_province, :type=>String
  key :start_city, :type=>String
  key :end_province, :type=>String
  key :end_city, :type=>String
  key :count, :type=>Integer


end