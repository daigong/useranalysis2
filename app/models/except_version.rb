#encoding=utf-8

class ExceptVersion
  include Mongoid::Document
  store_in :mobile_exceptinfo_version

  key :count, :type => Integer
  key :log, :type => String
  key :key, :type => String
  key :soft_version_code, :type => Integer

end
