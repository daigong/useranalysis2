#encoding=utf-8

class ExceptDay
  include Mongoid::Document
  set_database :data_mining
  store_in :mobile_exceptinfo_day

  key :count, :type=>Integer
  key :log, :type=>String
  key :day_date, :type=>Time
  key :key, :type=>String

end
