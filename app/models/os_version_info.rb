#encoding=utf-8
class OsVersionInfo
  include Mongoid::Document
  store_in :mobile_osversioninfo

  key :key, :type=>String
  key :day_date, :type=>Time
  key :new_user_count, :type=>Integer
  key :login_user_count, :type=>Integer
  key :os_version, :type=>String

  def new_user_count
	if self[:new_user_count]==nil
		return 0
	else
		self[:new_user_count]
	end
  end
end
