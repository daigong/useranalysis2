#encoding=utf-8
class BaseInfo
  include Mongoid::Document
  store_in :mobile_baseinfo
  key :key, :type=>String
  key :day_date, :type=>Time
  key :sum_user_count, :type=>Integer
  key :sum_login_count, :type=>Integer
  key :new_user_count, :type=>Integer
  key :login_count, :type =>Integer
  key :login_user_count, :type=>Integer
  key :update_user_count, :type=>Integer
  key :degrade_user_count, :type=>Integer
  key :average_use_time, :type=>Integer
  key :average_traffics_up, :type=>Integer
  key :average_traffics_down, :type=>Integer

  key :active_7day_user_count, :type=>Integer
  key :active_14day_user_count, :type=>Integer
  key :active_30day_user_count, :type=>Integer
  key :active_60day_user_count, :type=>Integer
  key :active_90day_user_count, :type=>Integer
  key :active_180day_user_count, :type=>Integer

  def active_7day_rate
    self.active_7day_user_count.to_f / self.sum_user_count.to_f
  end
  def active_14day_rate
    self.active_14day_user_count.to_f / self.sum_user_count.to_f
  end
  def active_30day_rate
    self.active_30day_user_count.to_f / self.sum_user_count.to_f
  end
  def active_60day_rate
    self.active_60day_user_count.to_f / self.sum_user_count.to_f
  end
  def active_90day_rate
    self.active_90day_user_count.to_f / self.sum_user_count.to_f
  end
  def active_180day_rate
    self.active_180day_user_count.to_f / self.sum_user_count.to_f
  end
  #获得实时信息，该信息是实时读取
  def self.get_day_info key, day
    #today_info = {}
    #today_info[:key] = key
    #today_info[:day_date] = day
    #today_info[:sum_user_count] = User.where(:key=>key, :history_login_time.lte=>day+1.day).count
    #today_info[:sum_login_count] = LoginInfo.where(:key=>key, :time.lte=>day+1.day).count
    #today_info[:new_user_count] = User.where(:key=>key, :first_login_time.gte=>day, :first_login_time.lte=>day+1.day).count
    #today_info[:login_count] = LoginInfo.where(:key=>key, :time.gte=>day, :time.lte=>day+1.day).count
    #today_info[:login_user_count] = User.where(:key=>key, :history_login_time.gte=>day, :history_login_time.lte=>day+1.day).count
    #today_info[:update_user_count] = User.where(:key=>key, :"history_update.time".gte=>day, :"history_update.time".lte=>day+1.day).count;
    today_info = BaseInfo.where(:key=>key, :day_date=>day).first
    if today_info.nil?
      today_info={};
      today_info[:day_date] = 0
      today_info[:sum_user_count] = 0
      today_info[:sum_login_count] = 0
      today_info[:new_user_count] = 0
      today_info[:login_count] = 0
      today_info[:login_user_count] = 0
      today_info[:update_user_count] = 0
      today_info[:degrade_user_count] = 0
    end
    return today_info
  end

  #通过日期，查询该天按小时分布新增用户数量,day要求是当天的00：00
  def self.new_user_count_by_hour_info day, app_key
    hour_info = [];
    (0...24).each do |num|
      begin_time = day+num.hour
      end_time = day+(num+1).hour
      hour_info<<User.where(:key=>app_key, :first_login_time.gte=>begin_time, :first_login_time.lte=>end_time).count
    end
    return hour_info
  end

  #通过日期，查询该天按小时分布用户启动数量，day要求是当天的00：00
  def self.login_count_by_hour_info day, app_key
    hour_info = [];
    (0...24).each do |num|
      begin_time = day+num.hour
      end_time = day+(num+1).hour
      hour_info<<LoginInfo.where(:key=>app_key, :time.gte=>begin_time, :time.lte=>end_time).count
    end
    return hour_info
  end

  #每日新增用户
  #begin_day开始，end_day结束（统计包括这俩天）
  def self.get_by_day begin_day, end_day, app_key
    #day_info = [];
    #while begin_day<=end_day
    #  day_info<<BaseInfo.where(:key=>app_key, :day_date=>begin_day).first
    #  begin_day+=1.day
    #end
    #return day_info
    BaseInfo.where(:key=>app_key, :day_date.gte=>begin_day, :day_date.lte=>end_day).order_by(:day_date, :asc)
  end

  #获得所有产品的人数信息
  def self.get_total_by_day begin_day, end_day
    day_info = [];
    while begin_day<=end_day
      day_info<<{
          :new_user_count=>BaseInfo.where(:day_date=>begin_day).sum(:new_user_count),
          :login_user_count=>BaseInfo.where(:day_date=>begin_day).sum(:login_user_count),
          :login_count=>BaseInfo.where(:day_date=>begin_day).sum(:login_count),
      };
      begin_day+=1.day
    end
    return day_info;
  end

  def method_missing argc

    begin
      return_val = super argc
    rescue Exception => e
      return_val = 0
    end
    return return_val

  end
end
