#encoding=utf-8
class ChannelDeviceInfo
  include Mongoid::Document
  store_in :mobile_deviceinfo_channel
  key :key, :type=>String
  key :day_date, :type=>Time
  key :channel, :type=>String
  key :new_user_count, :type=>Integer
  key :device_model, :type =>String

  def self.get_total_by_day day, chan, app_key, page
    device_info = {};
    devices = ChannelDeviceInfo.where(:day_date=>day, :key=>app_key, :channel=>chan).order_by(:new_user_count, :desc).paginate(:per_page => 5,:page=>page)
    devices.each do |d|
      if d.device_model.eql? ""
        device_info["UNKNOW"]=d.new_user_count
      else
        device_info[d.device_model]=d.new_user_count
      end
    end
    return device_info;
  end
end