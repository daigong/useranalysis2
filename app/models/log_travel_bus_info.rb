#encoding=utf-8
class LogTravelBusInfo
  include Mongoid::Document
  set_database :log
  store_in :travel_bus
  key :day_date, :type=>Time
  key :province, :type=>String
  key :city, :type=>String
  key :count, :type=>Integer


end