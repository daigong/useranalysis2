#encoding=utf-8
class Service::UserInfoServiceController < ApplicationController

  layout false, :only=>:get_user_info

  ####获得用户信息##################################
  def get_user_info

    uuid = params[:uuid]
    key = params[:key]

    if key.nil?
	@user = LoginInfo.where(:uuid=>uuid).order_by(:time,:desc).first
    else
	@user = LoginInfo.where(:key=>key,:uuid=>uuid).order_by(:time,:desc).first
    end


    unless @user.nil?
	    @user[:lat]||=0
	    @user[:lon]||=0
	    @user[:last_login_lat]=@user[:lat]
	    @user[:last_login_lon]=@user[:lon]
    end

    @jsonp ="var data="+@user.to_json+";"

    render :json=>@jsonp

  end

end
