#encoding=utf-8
class Loganalysis::PoiAroundController < Loganalysis::BaseController
  def index
    @day = DateUtils.today-1.day

    unless params[:day].nil?
      @day = DateUtils.format_day params[:day]
    end

    @page_info = LogPoiAroundInfo.where(:day_date=>@day).order_by(:count, :desc).paginate(:per_page => 20, :page=>params[:page])


  end
end
