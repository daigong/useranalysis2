#encoding=utf-8
class Loganalysis::AnalysisController < Loganalysis::BaseController


  layout 'loganalysis/layout_left_menu'
  
  def index

    @day = DateUtils.today-1.day

    unless params[:day].nil?
      @day = DateUtils.format_day params[:day]
    end

    @sum_percentage_data = LogSumInfo.get_percentage_day_info(@day)

    @sum_count_data = LogSumInfo.get_count_day_info(@day)

    @sum_types_pie = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"\n"};
      f.options[:chart][:width]=700
      f.options[:chart][:height]=300
      f.options[:chart][:defaultSeriesType] = "pie"
      f.series(:name=>'检索类型', :data=>@sum_percentage_data)
    end

    begin_day = @day - 30.days

    end_day = @day

    count_by_day = LogSumInfo.get_count_info(begin_day, end_day)

    @count_by_daily_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"查询次数的月变化趋势"};
      f.options[:chart][:width]=700
      f.options[:chart][:defaultSeriesType] = "spline"
      f.options[:legend][:layout]=:horizontal
      f.options[:xAxis][:categories]= DateUtils.get_day_string(begin_day, end_day)
      f.options[:xAxis][:labels]={"step" =>3, "align" =>"right", "rotation" => -90}
      f.options[:yAxis][:min]= 0
      count_by_day.keys.each do |key|
        f.series(:name=>key, :data=>count_by_day[key])
      end
    end

    

  end
end
