#encoding=utf-8
class Loganalysis::BusstopController < Loganalysis::BaseController

  def index
    @day = DateUtils.today-1.day

    unless params[:day].nil?
      @day = DateUtils.format_day params[:day]
    end

    busstop_count_top_10 = LogBusstopInfo.where(:day_date=>@day).order_by(:count, :desc).limit(10)

    count_top_10_categories = []

    count_top_10_data = []

    busstop_count_top_10.each do |info|
      count_top_10_categories<<DataUtils.concat_string(info.province, info.city)
      count_top_10_data<<info.count
    end

    @busstop_count_top_10_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"公交站点查询次数 TOP 10"};
      f.options[:chart][:width]=700
      f.options[:chart][:defaultSeriesType] = "bar"
      f.options[:xAxis][:categories]= count_top_10_categories
      f.options[:plotOptions]=
          {
              :bar=> {
                  :dataLabels=>{
                      :enabled=>true
                  }
              }
          };
      f.series(:name=>'公交站点查询次数', :data=>count_top_10_data)
    end

    @page_info = LogBusstopInfo.where(:day_date=>@day, :city.exists => true).order_by(:count, :desc).paginate(:per_page => 10, :page=>params[:page])
    
  end

  def busstop_details_page
    @day = DateUtils.today-1.day

    unless params[:day].nil?
      @day = DateUtils.format_day params[:day]
    end

    @page_info = LogBusstopInfo.where(:day_date=>@day, :city.exists => true).order_by(:count, :desc).paginate(:per_page => 20, :page=>params[:page])
  end

end
