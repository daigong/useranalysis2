class Statistics::ExceptInfoController < Statistics::BaseController

  def index

    @page_info = ExceptInfo.where(:key=>params[:app_id])

    unless params[:soft_version].blank?
      @page_info = @page_info.where(:soft_version=>params[:soft_version]);
    end

    @page_info = @page_info.order_by(:time,:desc)

    @page_info = @page_info.paginate(:per_page => 7, :page => params[:page])

  end

end
