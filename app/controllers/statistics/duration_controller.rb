#encoding=utf-8
class Statistics::DurationController < Statistics::BaseController
  def index
    @column_stacking_demo = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"用户信息图表"};
      f.options[:chart][:width]=700
      f.options[:chart][:defaultSeriesType] = "column"
      f.options[:xAxis][:categories]= ["01-03", "01-04", "01-05", "01-06", "01-07"]
      f.options[:xAxis][:labels]={"step" =>3, "align" =>"right", "rotation" => -90}
      f.options[:plotOptions]={"column"=>{"stacking"=>"normal"}}
      counts = [2369, 2266, 2266, 2152, 1054]
      f.series(:name=>'今日', :data=>counts)
      counts2 = counts.collect do |count|
        count + 1000;
      end
      f.series(:name=>'昨日', :data=>counts2)
      counts3 = counts.collect do |count|
        count + 1000;
      end
      f.series(:name=>'前日', :data=>counts3)
    end

    @bar_demo = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"用户信息图表"};
      f.options[:chart][:width]=700
      f.options[:chart][:defaultSeriesType] = "bar"
      f.options[:xAxis][:categories]= ["广东", "江苏", "浙江", "河北", "湖北", "福建", "山东"]
      counts = [14.6, 8.4, 6.2, 5.0, 5.0, 4.8, 4.7]
      f.options[:plotOptions]=
          {
              :bar=> {
                  :dataLabels=>{
                      :enabled=>true
                  }
              }
          };
      f.series(:name=>'今日(%)', :data=>counts)
    end
  end
end
