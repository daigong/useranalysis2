class Statistics::ExceptDayController < Statistics::BaseController

  def index

    @page_info = ExceptDay.where(:key=>params[:app_id])

    @today = DateUtils.today-1.day

    unless params[:day_date].blank?
      @page_info = @page_info.where(:day_date => DateUtils.format_day(params[:day_date]));
    else
      @page_info = @page_info.where(:day_date=>@today);
      params[:day_date]=DateUtils.format_date_to_string(@today)
    end

    @page_info = @page_info.order_by(:count,:desc)

    @page_info = @page_info.paginate(:per_page => 7, :page => params[:page])
  end

end
