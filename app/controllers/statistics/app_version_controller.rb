#encoding=utf-8
#版本分布
class Statistics::AppVersionController < Statistics::BaseController
  def index

    @day = DateUtils.today - 1.day
    #实d时获得全部版本信息
    @now_version_infos = VersionInfo.all_version_info params[:app_id],@day
  
    @day_result = BaseInfo.get_day_info(params[:app_id],@day)
    #所有版本用户数量
    @total_user_count = @day_result[:sum_user_count]
    #今天启动用户数量
    @today_user_login_count = @day_result[:login_user_count];
    #昨天启动用户数量
    @yesterday_user_login_count = BaseInfo.get_day_info(params[:app_id],@day-1.day)[:login_user_count]

    @version_infos = VersionInfo.where(:key=>params[:app_id], :day_date=>@day).order_by(:new_user_count, :desc).limit(5);
    #{
    # :version=>['2.1','2.2','2.3']
    # :count=>[[1,2,3],[1,2,3],[1,2,3]]
    #count=>[记录日期]
    #}
    #
    top_5_version_info = {
        :version=>[],
        :count=>[],
        :login=>[]
    }

    5.times do |index|
      #将前五version放入top_5_version_info，如果不足5个，能放入多少是多少
      if @version_infos.length >= index+1
        unless top_5_version_info[:version].include?(@version_infos[index].soft_version)
          top_5_version_info[:version]<<@version_infos[index].soft_version
        end
      end
    end
    @begin_time = @day-15.day
    @end_time = @day

    top_5_version_info[:version].each do |version|
      version_count_by_day = []
      version_login_by_day = []
      temp_time=@begin_time
      while temp_time<=@end_time do
        version_info = VersionInfo.where(:key=>params[:app_id], :day_date=>temp_time,:soft_version=>version).first
        if version_info.nil?
          version_count_by_day<<0
          version_login_by_day<<0
        else
          version_count_by_day<<version_info.new_user_count rescue 0
          version_login_by_day<<version_info.login_user_count rescue 0
        end
        temp_time+=1.day
      end
      top_5_version_info[:count]<<version_count_by_day
      top_5_version_info[:login]<<version_login_by_day
    end

    @new_user_by_daily_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"新增用户 TOP 5 版本变化趋势"};
      f.options[:chart][:width]=700
      f.options[:chart][:defaultSeriesType] = "spline"
      f.options[:legend][:layout]=:horizontal
      f.options[:xAxis][:categories]= DateUtils.get_day_string(@begin_time, @end_time)
      f.options[:xAxis][:labels]={"step" =>3, "align" =>"right", "rotation" => -90}
      f.options[:yAxis][:min]= 0
      index = 0
      top_5_version_info[:version].each do |version|
        f.series(:name=>version, :data=>top_5_version_info[:count][index])
        index += 1;
      end
    end

    @login_by_daily_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"活跃用户 TOP 5 版本变化趋势"};
      f.options[:chart][:width]=700
      f.options[:chart][:defaultSeriesType] = "spline"
      f.options[:legend][:layout]=:horizontal
      f.options[:xAxis][:categories]= DateUtils.get_day_string(@begin_time, @end_time)
      f.options[:xAxis][:labels]={"step" =>3, "align" =>"right", "rotation" => -90}
      f.options[:yAxis][:min]= 0
      index = 0
      top_5_version_info[:version].each do |version|
        f.series(:name=>version, :data=>top_5_version_info[:login][index])
        index += 1;
      end
    end
  end
end
