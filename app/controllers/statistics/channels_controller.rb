#encoding=utf-8
class Statistics::ChannelsController < Statistics::BaseController
  def index

    day = DateUtils.today-1.day

    @day =day

    begin_day = day-15.day

    end_day = day

    @channels_page_info = ChannelInfo.where(:day_date => day, :key => params[:app_id], :channel.exists => true, :channel.ne => "").order_by(:new_user_count, :desc).paginate(:per_page => 7, :page => params[:page])



    channels_every_day_infos = {}

    @channels_page_info.each do |chan|
      unless channels_every_day_infos.include?(chan.channel)
        channels_every_day_infos[chan.channel]= ChannelInfo.get_total_by_day(begin_day, end_day, chan.channel, params[:app_id])
      end
    end

    new_user_count_data = {};
    login_user_count_data = {};
    login_count_data = {};
    average_use_time = {};


    channels_every_day_infos.keys.each do |key|
      new_user_count_data[key]=[]
      login_user_count_data[key]=[]
      login_count_data[key]=[]
      average_use_time[key]=[]

      check_day =begin_day

      ##处理begin->end之间没有数据的渠道(添加0)
      #unless channels_every_day_infos[key][0].nil?
      #  while check_day != channels_every_day_infos[key][0][:day_date]
      #    new_user_count_data[key]<<0
      #    login_user_count_data[key]<< 0
      #    login_count_data[key]<< 0
      #    average_use_time[key]<< 0
      #    check_day=check_day+1.day
      #  end
      #end

      channels_every_day_infos[key].each do |value|

        new_user_count_data[key]<<value[:new_user_count]
        login_user_count_data[key]<< value[:login_user_count]
        login_count_data[key]<< value[:login_count]
        average_use_time[key]<< value[:average_use_time]
      end
      
      new_user_count_data[key].reverse!
      login_user_count_data[key].reverse!
      login_count_data[key].reverse!
       average_use_time[key].reverse!
    end



    @installations_by_daily_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text => "\n"};
      f.options[:chart][:width]=700
      f.options[:chart][:height]=300
      f.options[:chart][:defaultSeriesType] = "spline"
      f.options[:legend][:layout]=:horizontal
      f.options[:xAxis][:categories]= DateUtils.get_day_string(begin_day, end_day)
      f.options[:xAxis][:labels]={"step" => 3, "align" => "right", "rotation" => -90}
      f.options[:yAxis][:min]= 0
      index = 0
      new_user_count_data.keys.each do |key|
        f.series(:name => key, :data => new_user_count_data[key])
        index += 1;
      end
    end

    @total_installations_by_daily_chart= LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text => "\n"};
      f.options[:chart][:width]=700
      f.options[:chart][:height]=300
      f.options[:chart][:defaultSeriesType] = "spline"
      f.options[:legend][:layout]=:horizontal
      f.options[:xAxis][:categories]= DateUtils.get_day_string(begin_day, end_day)
      f.options[:xAxis][:labels]={"step" => 3, "align" => "right", "rotation" => -90}
      f.options[:yAxis][:min]= 0
      index = 0
      login_user_count_data.keys.each do |key|
        f.series(:name => key, :data => login_user_count_data[key])
        index += 1;
      end
    end

    @launches_by_daily_chart=LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text => "\n"};
      f.options[:chart][:width]=700
      f.options[:chart][:height]=300
      f.options[:chart][:defaultSeriesType] = "spline"
      f.options[:legend][:layout]=:horizontal
      f.options[:xAxis][:categories]= DateUtils.get_day_string(begin_day, end_day)
      f.options[:xAxis][:labels]={"step" => 3, "align" => "right", "rotation" => -90}
      f.options[:yAxis][:min]= 0
      index = 0
      login_count_data.keys.each do |key|
        f.series(:name => key, :data => login_count_data[key])
        index += 1;
      end
    end
    @average_use_time_by_daily_chart=LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text => "\n"};
      f.options[:chart][:width]=700
      f.options[:chart][:height]=300
      f.options[:chart][:defaultSeriesType] = "spline"
      f.options[:legend][:layout]=:horizontal
      f.options[:xAxis][:categories]= DateUtils.get_day_string(begin_day, end_day)
      f.options[:xAxis][:labels]={"step" => 3, "align" => "right", "rotation" => -90}
      f.options[:yAxis][:min]= 0
      index = 0
      average_use_time.keys.each do |key|
        f.series(:name => key, :data => average_use_time[key])
        index += 1;
      end
    end
  end
end
