#encoding=utf-8
class Statistics::UsetimeController < Statistics::BaseController
  def index
    @day = DateUtils.today-1.day
    once_usetime_info = OnceUsetimeInfo.where(:key=>params[:app_id], :day_date=>@day).first

    total = once_usetime_info['use_time_level0']+once_usetime_info['use_time_level1']+
        once_usetime_info['use_time_level2']+once_usetime_info['use_time_level3']+
        once_usetime_info['use_time_level4']+once_usetime_info['use_time_level5']+
        once_usetime_info['use_time_level6']+once_usetime_info['use_time_level7']


    @once_usetime_distribution_bar_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"单次使用时长"};
      f.options[:chart][:width]=700
      f.options[:chart][:defaultSeriesType] = "bar"
      f.options[:xAxis][:categories]= ['0-3秒', '3-10秒', '10-30秒', '30-60秒', '1-3分钟', '3-10分钟', '10-30分钟', '30+分钟']
      f.options[:plotOptions]=
          {
              :bar=> {
                  :dataLabels=>{
                      :enabled=>true
                  }
              }
          };
      counts = [
          CalcUtils.calc_percentage_to_i(once_usetime_info.use_time_level0, total, 1),
          CalcUtils.calc_percentage_to_i(once_usetime_info.use_time_level1, total, 1),
          CalcUtils.calc_percentage_to_i(once_usetime_info.use_time_level2, total, 1),
          CalcUtils.calc_percentage_to_i(once_usetime_info.use_time_level3, total, 1),
          CalcUtils.calc_percentage_to_i(once_usetime_info.use_time_level4, total, 1),
          CalcUtils.calc_percentage_to_i(once_usetime_info.use_time_level5, total, 1),
          CalcUtils.calc_percentage_to_i(once_usetime_info.use_time_level6, total, 1),
          CalcUtils.calc_percentage_to_i(once_usetime_info.use_time_level7, total, 1)
      ]
      f.series(:name=>"#{DateUtils.format_date_to_string(@day)}", :data=>counts)
    end

    day_usetime_infos = DayUsetimeInfo.where(:key=>params[:app_id], :day_date=>@day).first

    total = day_usetime_infos['use_time_level0']+ day_usetime_infos['use_time_level1']+
        day_usetime_infos['use_time_level2']+day_usetime_infos['use_time_level3']+
        day_usetime_infos['use_time_level4']+day_usetime_infos['use_time_level5']+
        day_usetime_infos['use_time_level6']+day_usetime_infos['use_time_level7']

    @day_usetime_distribution_bar_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"日使用时长"};
      f.options[:chart][:width]=700
      f.options[:chart][:defaultSeriesType] = "bar"
      f.options[:xAxis][:categories]= ['0-3秒', '3-10秒', '10-30秒', '30-60秒', '1-3分钟', '3-10分钟', '10-30分钟', '30+分钟']
      f.options[:plotOptions]=
          {
              :bar=> {
                  :dataLabels=>{
                      :enabled=>true
                  }
              }
          };
      counts = [
          CalcUtils.calc_percentage_to_i(day_usetime_infos.use_time_level0, total, 1),
          CalcUtils.calc_percentage_to_i(day_usetime_infos.use_time_level1, total, 1),
          CalcUtils.calc_percentage_to_i(day_usetime_infos.use_time_level2, total, 1),
          CalcUtils.calc_percentage_to_i(day_usetime_infos.use_time_level3, total, 1),
          CalcUtils.calc_percentage_to_i(day_usetime_infos.use_time_level4, total, 1),
          CalcUtils.calc_percentage_to_i(day_usetime_infos.use_time_level5, total, 1),
          CalcUtils.calc_percentage_to_i(day_usetime_infos.use_time_level6, total, 1),
          CalcUtils.calc_percentage_to_i(day_usetime_infos.use_time_level7, total, 1)
      ]
      f.series(:name=>"#{DateUtils.format_date_to_string(@day)}", :data=>counts)
    end


    week_usetime_info = WeekUsetimeInfo.where(:key=>params[:app_id], :day_date=>@day).first

    total = week_usetime_info['use_time_level0']+ week_usetime_info['use_time_level1']+
        week_usetime_info['use_time_level2']+week_usetime_info['use_time_level3']+
        week_usetime_info['use_time_level4']+week_usetime_info['use_time_level5']+
        week_usetime_info['use_time_level6']+week_usetime_info['use_time_level7']+week_usetime_info['use_time_level8']


    @week_usetime_distribution_bar_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"周使用时长"};
      f.options[:chart][:width]=700
      f.options[:chart][:defaultSeriesType] = "bar"
      f.options[:xAxis][:categories]= ['0-30秒', '30-60秒', '1-3分钟', '3-10分钟', '10-30分钟', '30-60分钟', '60-90分钟', '90-120分钟', '120+分钟']
      f.options[:plotOptions]=
          {
              :bar=> {
                  :dataLabels=>{
                      :enabled=>true
                  }
              }
          };
      counts = [
          CalcUtils.calc_percentage_to_i(week_usetime_info.use_time_level0, total, 1),
          CalcUtils.calc_percentage_to_i(week_usetime_info.use_time_level1, total, 1),
          CalcUtils.calc_percentage_to_i(week_usetime_info.use_time_level2, total, 1),
          CalcUtils.calc_percentage_to_i(week_usetime_info.use_time_level3, total, 1),
          CalcUtils.calc_percentage_to_i(week_usetime_info.use_time_level4, total, 1),
          CalcUtils.calc_percentage_to_i(week_usetime_info.use_time_level5, total, 1),
          CalcUtils.calc_percentage_to_i(week_usetime_info.use_time_level6, total, 1),
          CalcUtils.calc_percentage_to_i(week_usetime_info.use_time_level7, total, 1),
          CalcUtils.calc_percentage_to_i(week_usetime_info.use_time_level8, total, 1)
      ]
      f.series(:name=>"#{DateUtils.format_date_to_string(@day-7.day)}~#{DateUtils.format_date_to_string(@day)}", :data=>counts)
    end

  end
end