#encoding=utf-8
class Statistics::DebugInfoController < Statistics::BaseController

  def index

    @page_info = DebugInfo.where(:key=>params[:app_id]).where(:level => 'debug')

    unless params[:soft_version].blank?
      @page_info = @page_info.where(:soft_version=>params[:soft_version]);
    end

    unless params[:uuid].blank?
      @page_info = @page_info.where(:uuid => params[:uuid])
    end

    @page_info = @page_info.order_by(:time,:desc)

    @page_info = @page_info.paginate(:per_page => 15, :page => params[:page])

  end

end
