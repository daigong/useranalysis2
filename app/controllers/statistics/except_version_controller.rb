class Statistics::ExceptVersionController < Statistics::BaseController

  def index
    @page_info = ExceptVersion.where(:key => params[:app_id])

    unless params[:soft_version_code].blank?
      @page_info = @page_info.where(:soft_version_code=>params[:soft_version_code].to_i);
    end

    @page_info = @page_info.order_by(:soft_version_code, :desc)

    @page_info = @page_info.order_by(:count, :desc)

    @page_info = @page_info.paginate(:per_page => 7, :page => params[:page])
  end

end
