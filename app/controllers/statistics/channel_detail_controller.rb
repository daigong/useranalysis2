#encoding=utf-8
class Statistics::ChannelDetailController < Statistics::BaseController
  def index

    day = DateUtils.today-1.day

    begin_day = day-15.day

    end_day = day

    total_result = ChannelInfo.get_total_by_day(begin_day, end_day, params[:channel_id], params[:app_id])

    #每日新用户趋势
    new_user_count_infos = [];
    #活跃用户
    login_user_count_infos = [];
    #启动次数
    login_count_infos = [];

    temp_date = begin_day

#    unless total_result[0].nil?
#
 #     while temp_date != total_result[0][:day_date] do
  #      new_user_count_infos<<0
   #     login_user_count_infos<<0
    #    login_count_infos<<0
     #   temp_date = temp_date + 1.day
    #  end

   # end

    total_result.each do |result|
      new_user_count_infos<<result[:new_user_count]
      login_user_count_infos<<result[:login_user_count]
      login_count_infos<<result[:login_count]
    end

    new_user_count_infos.reverse!
    login_user_count_infos.reverse!
    login_count_infos.reverse!

    @new_user_by_daily_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"\n"};
      f.options[:chart][:width]=700
      f.options[:chart][:height]=300
      f.options[:chart][:defaultSeriesType] = "spline"
      f.options[:xAxis][:categories]= DateUtils.get_day_string(begin_day, end_day)
      f.options[:xAxis][:labels]={"step" =>3}
      f.series(:name=>params[:channel_id], :data=>new_user_count_infos)
    end

    @active_users_by_daily_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"\n"};
      f.options[:chart][:width]=700
      f.options[:chart][:height]=300
      f.options[:chart][:defaultSeriesType] = "spline"
      f.options[:xAxis][:categories]= DateUtils.get_day_string(begin_day, end_day)
      f.options[:xAxis][:labels]={"step" =>3}
      f.series(:name=>params[:channel_id], :data=>login_user_count_infos)
    end

    @login_count_by_daily_chart = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"\n"};
      f.options[:chart][:width]=700
      f.options[:chart][:height]=300
      f.options[:chart][:defaultSeriesType] = "spline"
      f.options[:xAxis][:categories]= DateUtils.get_day_string(begin_day, end_day)
      f.options[:xAxis][:labels]={"step" =>3}
      f.series(:name=>params[:channel_id], :data=>login_count_infos)
    end

    channel_device_page_info = ChannelDeviceInfo.get_total_by_day(day, params[:channel_id], params[:app_id], params[:page])


    channel_region_page_info = ChannelRegionInfo.get_total_by_day(day, params[:channel_id], params[:app_id], params[:page])


    channel_version_page_info = ChannelVersionInfo.get_total_by_day(day, params[:channel_id], params[:app_id], params[:page])


    @new_user_percentage_device_chart =LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"\n"};
      f.options[:chart][:width]=700
      f.options[:chart][:defaultSeriesType] = "bar"
      f.options[:xAxis][:categories]= channel_device_page_info.keys
      f.options[:plotOptions]=
          {
              :bar=> {
                  :dataLabels=>{
                      :enabled=>true
                  }
              }
          };
      f.series(:name=>'设备', :data=>channel_device_page_info.values)
    end

    @new_user_percentage_region_chart =LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"\n"};
      f.options[:chart][:width]=700
      f.options[:chart][:defaultSeriesType] = "bar"
      f.options[:xAxis][:categories]= channel_region_page_info.keys
      f.options[:plotOptions]=
          {
              :bar=> {
                  :dataLabels=>{
                      :enabled=>true
                  }
              }
          };
      f.series(:name=>'省市', :data=>channel_region_page_info.values)
    end

    @new_user_percentage_version_chart =LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"\n"};
      f.options[:chart][:width]=700
      f.options[:chart][:defaultSeriesType] = "bar"
      f.options[:xAxis][:categories]= channel_version_page_info.keys
      f.options[:plotOptions]=
          {
              :bar=> {
                  :dataLabels=>{
                      :enabled=>true
                  }
              }
          };
      f.series(:name=>'版本', :data=>channel_version_page_info.values)
    end

  end

  #更多详细信息分页
  def more_details_page
    day = DateUtils.today-1.day

    @flag = params[:flag]

    @page_info = []
    if @flag.to_i == 1
      @page_info = ChannelDeviceInfo.where(:key=>params[:app_id], :day_date=>day, :channel=>params[:channel_id]).
          order_by(:new_user_count, :desc).paginate(:per_page => 20, :page=>params[:page]);
    end

    if @flag.to_i == 2
      @page_info = ChannelRegionInfo.where(:key=>params[:app_id], :day_date=>day, :channel=>params[:channel_id]).
          order_by(:new_user_count, :desc).paginate(:per_page => 20, :page=>params[:page]);
    end

    if @flag.to_i == 3
      @page_info = ChannelVersionInfo.where(:key=>params[:app_id], :day_date=>day, :channel=>params[:channel_id]).
          order_by(:new_user_count, :desc).paginate(:per_page => 20, :page=>params[:page]);
    end

  end
end
