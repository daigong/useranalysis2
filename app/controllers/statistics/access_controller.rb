#encoding=utf-8
class Statistics::AccessController < Statistics::BaseController
  def index

    @day = DateUtils.today

    @connect_types = LoginInfo.where(:key=>params[:app_id]).distinct(:connect_type);

    # like [["2G", 613958], ["WIFI", 275524], ["3G", 186123]]
    @results = [];

    @connect_types.each do |type|
      break if type.blank?
      result = [type]
      result<<LoginInfo.where(:key=>params[:app_id],:connect_type=>type, :time.lte=>@day).count
      @results<<result
    end

    @connect_types_pie = LazyHighCharts::HighChart.new('line') do |f|
      f.options[:title]={:text=>"联网方式"};
      f.options[:chart][:width]=700
      f.options[:chart][:defaultSeriesType] = "pie"
      f.series(:name=>'联网方式', :data=>@results)
    end
  end
end
